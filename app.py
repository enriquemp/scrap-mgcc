import requests  # module to make GET and POST requests
from bs4 import BeautifulSoup  # html scraper

# datos del Bot
token = "None"
channel_id = "7002674"

with open("/app/magic/token.txt", "r") as f:
    token = f.read().strip()

# this is our objective
url = "https://mtgfestivals.com"
# take care of headers from your petition, to avoid block from some sites
headers = {
    "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36"
}

# main request for the url
res = requests.get(url, headers=headers)

# preparing a soup
soup = BeautifulSoup(res.text, "html.parser")

# start filtering the content
div_bcn = soup.find("div", id="Barcelona")
# deeper filtering
bcn_info = div_bcn.find_next("span", class_="text--xlarge").text

# Telegram options to compose the message
parser_telegram = "markdown"
mensaje = (
    f"https://api.telegram.org/bot{token}/sendMessage?chat_id={channel_id}&parse_mode="
    f"{parser_telegram}&text="
    "Esta es la información actualizada sobre la MagicCon de Barcelona. \n"
    f"\n`{bcn_info}`\n"
    f"\nSi las últimas palabras de este mensaje son *'coming soon'*, significa que"
    f" no ha habido cambios. \n"
    f"\n Si quieres comprobarlo por ti mismo, haz click "
    f"[aquí](https://mtgfestivals.com/#Barcelona)\n"
)

# Send the message, commented option is verbose in local system
# print(requests.get(mensaje))
requests.get(mensaje)
