#!/usr/bin/env bash

# Grab the url and the pattern to search in this variables

theurl="https://www.mtgfestivals.com"
thestr="barcelona"

# Two kind of methods to get the source code, with 'w3m' we can get the source
# or the html parsed text.

# barcelona=$(curl "$theurl" | grep "$thestr")
scrap=$(w3m -dump_source "$theurl" | gunzip -f | grep -Ei "$thestr")

# Show the results in a desktop notification
msg="$scrap"
icon="./MTG-Festivals-logo.png"
notify-send -t 10000000 -i "$icon" "scrapeando magic festivals" "$msg"

# curl 'https://api.telegram.org/bot5768134093:AAHhoAXdcoj57kQxu5B2EtNql8QcRqDhuIs/sendMessage?chat_id=7002674&text='$msg'

BOT="5768134093:AAHhoAXdcoj57kQxu5B2EtNql8QcRqDhuIs"
channelID="7002674"
text="$scrap"
photo="https://1000marcas.net/wp-content/uploads/2021/05/Magic-The-Gathering-Logo.png"

# curl -X POST "https://api.telegram.org/bot$BOT/sendMessage"\
#     -d chat_id="$channelID" -d text="Found: $text"

curl -X POST "https://api.telegram.org/bot$BOT/sendPhoto"\
    -d chat_id="$channelID" -d text="Found: $text" -d photo="$photo"