FROM python:3.11-slim

WORKDIR /app/magic

COPY . . 
COPY cron-py /etc/cron.d/cron-py
# COPY requirements.txt requirements.txt

RUN apt-get update && apt-get install cron -y
RUN pip3 install -r requirements.txt
RUN touch /var/cron-py_log.log
RUN chmod 0744 /etc/cron.d/cron-py
RUN crontab /etc/cron.d/cron-py

CMD [ "cron", "-f" ]
