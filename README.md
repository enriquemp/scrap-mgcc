# Webpage sniffer [ WIP ]

This project is a simple sniffer to get updates upon a webpage of my son's interest.

Return the state of a "coming soon" announce. The script checks every morning the webpage and send us a Telegram message to know if there are some update or new info.

## INSTRUCTIONS TO BUILD DOCKER IMAGE

### Clone this repository

`git clone https://codeberg.org/enriquemp/scrap-mgcc.git`

### To set your Telegram Token

`cp token-sample.txt token.txt`
and set your `token` in the first line of that file.
